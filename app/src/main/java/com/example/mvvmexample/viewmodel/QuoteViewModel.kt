package com.example.mvvmexample.viewmodel

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.mvvmexample.model.Quote
import com.example.mvvmexample.model.QuoteProvider

class QuoteViewModel: ViewModel() {
    val quote = MutableLiveData<Quote>()

    fun randomQuote(){
        val newQuote = QuoteProvider.random()
        quote.postValue(newQuote)
    }
}