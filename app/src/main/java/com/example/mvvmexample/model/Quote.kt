package com.example.mvvmexample.model

data class Quote(val quote: String, val author: String)
