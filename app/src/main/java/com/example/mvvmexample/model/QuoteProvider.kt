package com.example.mvvmexample.model

class QuoteProvider {
    companion object{
        private val quotes = arrayOf<Quote>(
            Quote("Are you not entertained?", "Maximus"),
            Quote("You can have all the money in the world, but there’s one thing you will never have… a dinosaur.", "Homer Simpson"),
            Quote("I'm going to make him an offer he can't refuse.", "Don Vito Corleone"),
            Quote("I love the smell of napalm in the morning.", "Colonel Kilgore"),
            Quote("I'll be back.", "T-800"),
            Quote("My precious.", "Gollum"),
            Quote("Why so serious?", "The Joker"),
            Quote("I am Groot.", "Groot"),
            Quote("I want to play a game.", "Jigsaw"),
            Quote("This is Sparta!", "Leonidas")
        )
        fun random(): Quote {
            val i: Int = (0..quotes.size-1).random()
            return quotes[i]
        }
    }
}