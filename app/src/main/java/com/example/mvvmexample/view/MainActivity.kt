package com.example.mvvmexample.view

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.example.mvvmexample.databinding.ActivityMainBinding
import com.example.mvvmexample.viewmodel.QuoteViewModel
import androidx.activity.viewModels
import androidx.lifecycle.Observer

class MainActivity : AppCompatActivity() {

    lateinit var binding: ActivityMainBinding
    val quoteViewModel: QuoteViewModel by viewModels()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        quoteViewModel.quote.observe(this, Observer {
            binding.textViewQuote.text = it.quote
            binding.textViewAuthor.text = it.author
        })

        binding.nextButton.setOnClickListener({
            quoteViewModel.randomQuote()
        })
    }
}